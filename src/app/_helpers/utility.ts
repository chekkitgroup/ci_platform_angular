import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class UtilityProvider {

  constructor(
      public http: HttpClient,

      ) {

    }
   
    returnCurrencySymbol(c){
        let sym = '';

        if(c == 'NGN'){
            sym = '₦';
        }else if(c == 'USD'){
            sym = '$';
        }

        return sym;
    }
    
    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}
