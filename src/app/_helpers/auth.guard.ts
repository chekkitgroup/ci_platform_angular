import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '../_services';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        console.log("currentUser", currentUser);
        // if (currentUser) {
        //     console.log("currentUser>>>", currentUser);
        //     // authorised so return true
        //     return true;
        // }
        // console.log("currentUser>>>>>11", currentUser);
        // // not logged in so redirect to login page with the return url
        // this.router.navigate(['/pages/auth/login'], { queryParams: { returnUrl: state.url } });
        // return false;
        if (this.authenticationService.tokenExist()) {
            return true;
        } else {
            console.log('token value', '!toke exits');
            this.router.navigate(['/pages/auth/login']);
            return false;
        }
    }
}
