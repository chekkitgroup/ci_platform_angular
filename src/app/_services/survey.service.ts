import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Survey } from '../_models';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';

@Injectable({ providedIn: 'root' })
export class SurveyService {
    token: any;
    baseUrl = environment.apiUrl + environment.basePath;
    constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    }

    getAllUserSurveys(id: number) {
        return this.http.get(this.baseUrl + `/surveys/all_surveys/${id}`);
    }
    getAllUserLoyaltyPoints(id: number) {
        return this.http.get(this.baseUrl + `/surveys/all_loyaltypoints/${id}`);
    }
    getAllUserRaffle(id: number) {
        return this.http.get(this.baseUrl + `/surveys/get-user-raffles/${id}`);
    }
    getRaffleDetails(id: number) {
        return this.http.get(this.baseUrl + `/surveys/get-raffle-details/${id}`);
    }
    selectRaffleWinner(id: number) {
        return this.http.get(this.baseUrl + `/surveys/select-raffle-winner/${id}`);
    }
    getAllRedemptionPoints(id: number) {
        return this.http.get(this.baseUrl + `/surveys/all_redemptionpoints/${id}`);
    }
    addSurvey(surveydata: any) {
        return this.http.post(this.baseUrl + `/surveys/`, surveydata);
    }
    addLoyaltyPoint(loyaltyData: any) {
        return this.http.post(this.baseUrl + `/surveys/create-loyalty-point`, loyaltyData);
    }
    addRaffleDraw(raffleData: any) {
        return this.http.post(this.baseUrl + `/surveys/create-raffle-draw`, raffleData);
    }

    deleteRaffle(id: number) {
        return this.http.delete(this.baseUrl + `/surveys/delete-raffle/${id}`);
    }
    
    uploadGiftImage(imageData: any) {
        return this.http.post(this.baseUrl + `/surveys/upload-gift-image`, imageData);
    }
    updateSurvey(surveyId: number, surveydata: any) {
        return this.http.put(this.baseUrl + `/surveys/${surveyId}`, surveydata);
    }
    updateLoyalty(slug: number, loyaltyData: any) {
        return this.http.put(this.baseUrl + `/surveys/update-loyalty-point/${slug}`, loyaltyData);
    }
    updateProductSurvey(surveyId: number, productId: any) {
        console.log(surveyId);
        console.log(productId);
        return this.http.put(this.baseUrl + `/surveys/update-product/${surveyId}`, productId, {
            reportProgress: true,
            observe: 'events',
        });
    }
    updateProductSurvey2(surveyId: number, productId: any) {
        console.log(surveyId);
        console.log(productId);
        return this.http.put(this.baseUrl + `/surveys/update-product-survey/${surveyId}`, productId, {
            reportProgress: true,
            observe: 'events',
        });
    }
    getSurveyQuestions(id: number) {
        return this.http.get(this.baseUrl + `/surveyquestion/get-surveyquestions/${id}`);
    }
    getSurveyQuestionsWithResponses(id: number) {
        return this.http.get(this.baseUrl + `/surveyquestion/get-survey-questions-with-response/${id}`);
    }
    getLabelSurveyQuestionsWithResponses(id: number) {
        return this.http.get(this.baseUrl + `/surveyquestion/get-label-survey-questions-with-response/${id}`);
    }
    delete(id: number) {
        return this.http.delete(this.baseUrl + `/surveys/${id}`);
    }
}
