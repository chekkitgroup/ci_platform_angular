import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User, Survey, Product_Reward, Survey_Question, Product, RecordCounts } from '../_models';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    baseUrl = environment.apiUrl + environment.basePath;
    private currentUserSubject: BehaviorSubject<any>;
    private SurveySubject: BehaviorSubject<any>;
    private ProductRewardSubject: BehaviorSubject<any>;
    private SurveyQuestSubject: BehaviorSubject<any>;
    private ProductsSubject: BehaviorSubject<any>;
    private SubProductsSubject: BehaviorSubject<any>;
    private ProductPinsSubject: BehaviorSubject<any>;
    private RecordCountsSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    public allsurveys: Observable<any>;
    public allproductRewards: Observable<any>;
    public allsurveyQuestions: Observable<any>;
    public allProducts: Observable<any>;
    public allSubProducts: Observable<any>;
    public allUserProductPins: Observable<any>;
    public recordCounts: Observable<any>;



    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();

        this.SurveySubject = new BehaviorSubject<Survey>(JSON.parse(localStorage.getItem('allsurveys')));
        this.allsurveys = this.SurveySubject.asObservable();

        this.ProductRewardSubject = new BehaviorSubject<Product_Reward>(JSON.parse(localStorage.getItem('allsurveyRewards')));
        this.allproductRewards = this.ProductRewardSubject.asObservable();

        this.SurveyQuestSubject = new BehaviorSubject<Survey_Question>(JSON.parse(localStorage.getItem('allsurveyQuestions')));
        this.allsurveyQuestions = this.SurveyQuestSubject.asObservable();

        this.ProductsSubject = new BehaviorSubject<Product>(JSON.parse(localStorage.getItem('allProducts')));
        this.allProducts = this.ProductsSubject.asObservable();

        this.SubProductsSubject = new BehaviorSubject<Product>(JSON.parse(localStorage.getItem('allSubProducts')));
        this.allSubProducts = this.SubProductsSubject.asObservable();

        this.ProductPinsSubject = new BehaviorSubject<Product>(JSON.parse(localStorage.getItem('allProductPins')));
        this.allUserProductPins = this.ProductPinsSubject.asObservable();

        this.RecordCountsSubject = new BehaviorSubject<RecordCounts>(JSON.parse(localStorage.getItem('recordCounts')));
        this.recordCounts = this.RecordCountsSubject.asObservable();
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }
    public get surveysValues(): any {
        return this.SurveySubject.value;
    }
    login(username: string, password: string) {
        return this.http.post<any>(this.baseUrl + `/auth/signin`, { username, password })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user.data.user && user.data.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }
                return user;
            }));
    }
    /**
      * @param none
      * @returns boolean TRUE OR FALSE
      */
    tokenExist() {
        return !!JSON.parse(localStorage.getItem('currentUser'));
    }
    logout() {
        // remove user from local storage to log user out
        localStorage.clear();
        this.currentUserSubject.next(null);
    }
}
