import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeCollectionComponent } from './code-collection.component';

describe('CodeCollectionComponent', () => {
  let component: CodeCollectionComponent;
  let fixture: ComponentFixture<CodeCollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeCollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
