import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Overview',
    icon: 'home-outline',
    link: '/pages/overview',
  },
  {
    title: 'Analytics',
    icon: 'activity-outline',
    link: '/pages/analytics',
    home: true,
  },
  {
    title: 'Products',
    icon: 'cast-outline',
    link: '/pages/product',
    home: true,
  },
  {
    title: 'Insights',
    icon: 'bar-chart-2-outline',
    link: '/pages/insights',
    home: true,
  },
  {
    title: 'Surveys',
    icon: 'flip-outline',
    link: '/pages/survey',
    home: true,
  },
  {
    title: 'Rewards',
    icon: 'shield-outline',
    link: '/pages/rewards',
    home: true,
  },
  {
    title: 'Code Collections',
    icon: 'shopping-cart-outline',
    link: '/pages/code-collection',
    home: true,
  },
  {
    title: 'User Feedback Logs',
    icon: 'briefcase-outline',
    link: '/pages/user-feedback',
    home: true,
  },
  {
    title: 'Engage Customers',
    icon: 'people-outline',
    link: '/pages/retargeting',
    home: true,
  },
];

export const MENU_ITEMS1: NbMenuItem[] = [
  {
    title: 'Overview',
    icon: 'home-outline',
    link: '/pages/overview',
  },
  {
    title: 'Analytics',
    icon: 'activity-outline',
    link: '/pages/analytics',
    home: true,
  },
  {
    title: 'User Feedback Logs',
    icon: 'briefcase-outline',
    link: '/pages/user-feedback',
    home: true,
  },
  {
    title: 'Products',
    icon: 'cast-outline',
    link: '/pages/product',
    home: true,
  },
  {
    title: 'Insights',
    icon: 'bar-chart-2-outline',
    link: '/pages/insights',
    home: true,
  },
  {
    title: 'Surveys',
    icon: 'flip-outline',
    link: '/pages/survey',
    home: true,
  },
  {
    title: 'Rewards',
    icon: 'shield-outline',
    link: '/pages/rewards',
    home: true,
  },
];

export const MENU_ITEMS2: NbMenuItem[] = [
  {
    title: 'Overview',
    icon: 'home-outline',
    link: '/pages/overview',
  },
  {
    title: 'Engage Customers',
    icon: 'people-outline',
    link: '/pages/retargeting',
    home: true,
  },
];
