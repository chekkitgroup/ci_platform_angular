import { Component, OnInit } from '@angular/core';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';
import { AuthenticationService } from '../../_services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubscriptionService } from '../../_services/subscription.services';
import { first } from 'rxjs/operators';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import {UtilityProvider} from "../../_helpers/utility";

@Component({
  selector: 'ngx-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss'],
})
export class PaymentsComponent implements OnInit {
  reference = '';
  email: any;
  amount: any;
  allow_pay = true;
  confirm_btn = false;
  dis_form = false;
  currentUser: User;
  currentUserSubscription: Subscription;
  paymentForm: FormGroup;
  paymentData: any;
  index = 1;
  for_pin = false;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.BOTTOM_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';

  title = 'HI there!';
  content = `payment successful!`;

  types: NbComponentStatus[] = [
    'primary',
    'success',
    'info',
    'warning',
    'danger',
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  constructor(
    private FormBuilder: FormBuilder, 
    private authenticationService: AuthenticationService,
    private toastrService: NbToastrService, 
    private activatedRoute: ActivatedRoute,
    public utility: UtilityProvider,
    private _subService: SubscriptionService, 
    private router: Router) {
    this.paymentData = this.activatedRoute.snapshot.queryParams;
    console.log('this.paymentData.type', this.paymentData);
    if (this.paymentData.type == 'pin') {
      console.log('get to pin');
      this.for_pin = true;
    }
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
    });
  }

  ngOnInit() {
    this.paymentForm = this.FormBuilder.group({
      amount: [this.paymentData.amount, [Validators.required, Validators.maxLength(20), noWhitespaceValidator]],
      email: [this.paymentData.email, [Validators.required]],
    });
    this.reference = `ref-${Math.ceil(Math.random() * 10e13)}`;
  }
  onSubmit() {
    this.amount = Number(this.paymentForm.get('amount').value) * 100;
    this.email = this.paymentForm.get('email').value;
    this.allow_pay = false;
    this.confirm_btn = true;
    this.paymentForm.get('amount').disable();
    this.paymentForm.get('email').disable();
    console.log('how far');

  }
  async paymentInit() {
    console.log('Payment initialized');
  }
  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
  paymentDone(ref: any) {
    if (ref.status === 'success') {
      this._subService.verifySubscription(
        { userId: this.currentUser.id, amount: (this.amount / 100), ref: ref.reference, type: this.paymentData.type },
        this.paymentData.type).pipe(first()).subscribe(async data => {
          console.log('data>>>>', data);
          if (data['status']) {
            await this.showToast(this.status, this.title, this.paymentData.type + this.content);
            this.router.navigate(['pages/overview']);
          }
        });
    }
  }

  paymentCancel() {
    console.log('payment failed');
  }
}
