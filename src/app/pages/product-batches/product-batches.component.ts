import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ProductService } from '../../_services/product.service';
import { first } from 'rxjs/operators';
import { User } from '../../_models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../../_models/product';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-product-batches',
  templateUrl: './product-batches.component.html',
  styleUrls: ['./product-batches.component.scss']
})
export class ProductBatchesComponent implements OnInit {
  rows = [];
  temp = [];
  data: any;
  currentUser: User;
  productObjct: any;
  constructor(private productService: ProductService, private authenticationService: AuthenticationService,
    private activatedRoute: ActivatedRoute, private router: Router) {
    // if (!authenticationService.tokenExist()) {
    //   router.navigate(['pages/auth/login']);
    // }
    this.productObjct = JSON.parse(this.activatedRoute.snapshot.queryParams.data);
    this.get_Allbatches();
  }

  ngOnInit() {
  }
  get_Allbatches() {
    this.productService.getAllProductBatches(this.productObjct.id).pipe(first()).subscribe(data => {
      if (data['status']) {
        console.log(data['data'].productBatches);
        this.data = data['data'].productBatches;
        this.rows = this.data;
        this.temp = [...this.data];
      }
    });
  }
  gotoCreateBatch(productObjct: any) {
    let data = JSON.stringify(this.productObjct);
    this.router.navigate(['/pages/create-batch'], { queryParams: { data }, skipLocationChange: true });
  }
  ViewBatch(product: Product) {
    console.log('product', product);
    const data = JSON.stringify({
      batch: product,
      product: this.productObjct
    });
    this.router.navigate(['/pages/view-batch'], { queryParams: { data }, skipLocationChange: true });
  }
  gotoEditBatch(batchData: any) {
    console.log('yeshshs ', batchData);

    let data = JSON.stringify({
      batch: batchData
    });

    this.router.navigate(['/pages/edit-batch'], { queryParams: { data }, skipLocationChange: true });

  }
}
