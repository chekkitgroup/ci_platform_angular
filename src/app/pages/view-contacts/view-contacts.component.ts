import { Component, OnInit } from '@angular/core';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';
import { AuthenticationService } from '../../_services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from '../../_services/rest.services';
import { first } from 'rxjs/operators';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import {UtilityProvider} from "../../_helpers/utility";
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Papa } from 'ngx-papaparse';
import 'rxjs/Rx';
import { AlertService } from '../../_services';

import { ToastService } from '../../_services/toast.service';


@Component({
  selector: 'ngx-view-contacts',
  templateUrl: './view-contacts.component.html',
  styleUrls: ['./view-contacts.component.scss']
})
export class ViewContactsComponent implements OnInit {
  contacts: any;

  constructor(
    private FormBuilder: FormBuilder, 
    private authenticationService: AuthenticationService,
    private toastrService: NbToastrService, 
    private papa: Papa,
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute,
    public utility: UtilityProvider,
    private toastService: ToastService, 
    private _rest: RestService, 
    private router: Router
    ) { }

  ngOnInit() {
    this.getRetargetingContacts()
  }

  getRetargetingContacts() {
    this._rest.getRetargetingContacts().pipe(first()).subscribe(data => {
      // console.log(data['data']);
      if (data['status']) {
        this.contacts = data['data'];
        console.log(this.contacts)
      }
    });
  }
}
