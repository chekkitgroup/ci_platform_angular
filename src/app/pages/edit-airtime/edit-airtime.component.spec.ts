import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAirtimeComponent } from './edit-airtime.component';

describe('EditAirtimeComponent', () => {
  let component: EditAirtimeComponent;
  let fixture: ComponentFixture<EditAirtimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAirtimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAirtimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
