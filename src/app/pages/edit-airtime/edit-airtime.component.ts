import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../_services/product.service';
import { AuthenticationService, SurveyService, AlertService } from '../../_services';
import { first } from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { SurveyRewardService } from '../../_services/surveyReward.service';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';

@Component({
  selector: 'ngx-edit-airtime',
  templateUrl: './edit-airtime.component.html',
  styleUrls: ['./edit-airtime.component.scss']
})
export class EditAirtimeComponent implements OnInit {
  airtimeObjct: any;
  loading = false;
  currentUser: User;
  currentUserSubscription: Subscription;
  surveyRewardForm: FormGroup;
  constructor(private productService: ProductService, private formBuilder: FormBuilder, private router: Router,
    private authenticationService: AuthenticationService, private surveyService: SurveyService,
    private activatedRoute: ActivatedRoute, private alertService: AlertService,
    private surveyrewardServices: SurveyRewardService) {
    let dataIs = JSON.parse(this.activatedRoute.snapshot.queryParams.data);
    console.log("dataIs", dataIs.reward);
    this.airtimeObjct = dataIs.reward;
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
    });
  }

  ngOnInit() {
    this.surveyRewardForm = this.formBuilder.group({
      reward_value: [this.airtimeObjct.reward_value, [Validators.required,
        noWhitespaceValidator]],
      reward_type: ['Airtime', [Validators.required, noWhitespaceValidator]],
      reward_quantity: [this.airtimeObjct.reward_quantity, [Validators.required,
        noWhitespaceValidator]],
    });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.surveyRewardForm.get('photo').setValue(file);
    }
  }
  get f() { return this.surveyRewardForm.controls; }

  onSubmit() {
    console.log('get here');
    // stop here if form is invalid
    console.log(this.surveyRewardForm.invalid);
    if (this.surveyRewardForm.invalid) {
      return;
    }
    this.loading = true;
    console.log(this.surveyRewardForm.value);
    this.surveyrewardServices.updateAirtimeReward(this.airtimeObjct.id, this.surveyRewardForm.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log('ok>>', data['data']);
          if (data['status']) {
            this.loading = true;
            console.log('oka 3 >>', data['data']);
            // this.data = this.save_products;
            this.router.navigate(['/pages/all-rewards']);
          }
          else {
            this.loading = true;
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
