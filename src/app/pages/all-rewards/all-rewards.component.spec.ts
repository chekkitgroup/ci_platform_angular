import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllRewardsComponent } from './all-rewards.component';

describe('AllRewardsComponent', () => {
  let component: AllRewardsComponent;
  let fixture: ComponentFixture<AllRewardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllRewardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllRewardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
