import { Component, OnDestroy, ViewChild } from '@angular/core';
import { takeWhile } from 'rxjs/operators';

import { OrdersChartComponent } from './charts/orders-chart.component';
import { ProfitChartComponent } from './charts/profit-chart.component';
import { OrdersChart } from '../../../@core/data/orders-chart';
import { ProfitChart } from '../../../@core/data/profit-chart';
import { OrderProfitChartSummary, OrdersProfitChartData } from '../../../@core/data/orders-profit-chart';
import { ProductService } from '../../../_services/product.service';
import { User } from './../../../_models/user';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../../../_services';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'ngx-ecommerce-charts',
  styleUrls: ['./charts-panel.component.scss'],
  templateUrl: './charts-panel.component.html',
})
export class ECommerceChartsPanelComponent implements OnDestroy {
  private alive = true;
  private data = {};
  chartPanelSummary: OrderProfitChartSummary[];
  period: string = 'week';
  ordersChartData: OrdersChart;
  profitChartData: ProfitChart;
  currentUserSubscription: Subscription;
  currentUser: User;
  month_lineData = [];
  month_periods = [];
  month_rawPeriods = [];
  month_rawPeriods1 = [];
  month_rawPeriods2 = [];

  @ViewChild('ordersChart', { static: true }) ordersChart: OrdersChartComponent;
  @ViewChild('profitChart', { static: true }) profitChart: ProfitChartComponent;


  lineChartData: ChartDataSets[];

  lineChartLabels: Label[];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor(private authenticationService: AuthenticationService,
    private ordersProfitChartService: OrdersProfitChartData, private _productService: ProductService) {
    this.lineChartData = [
      { data: [], label: 'Generated Pins' },
      { data: [], label: 'Valid Pins' },
      { data: [], label: 'Used Pins' },
    ];
    this.lineChartLabels = [];
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
      _productService.getUserLineGRaphAnalytics(this.currentUser.id).pipe(takeWhile(() => this.alive))
        .subscribe((summary) => {
          const new_mnth_data = summary['data'].all_month_data;
          const new_mnth_data1 = summary['data'].valid_all_month_data;
          const new_mnth_data2 = summary['data'].invalid_all_month_data;
          this.computeMonthData(new_mnth_data, new_mnth_data1, new_mnth_data2);
        });
    });
    this.ordersProfitChartService.getOrderProfitChartSummary()
      .pipe(takeWhile(() => this.alive))
      .subscribe((summary) => {
        console.log('summary>>>>>', summary);
        this.chartPanelSummary = summary;
        this.getOrdersChartData(this.period);
        this.getProfitChartData(this.period);
      });
  }
  async computeMonthData(new_data, new_data1, new_data2) {
    await new_data.forEach(key => {
      this.month_periods.push(key.month_name);
      this.month_rawPeriods.push(key.value);
    });
    const objctData = { data: this.month_rawPeriods, label: 'Generated Pins' };
    await this.month_lineData.push(objctData);

    await new_data1.forEach(async key => {
      await this.month_rawPeriods1.push(key.value);
    });
    const objctData2 = { data: this.month_rawPeriods1, label: 'Valid Pins' };
    await this.month_lineData.push(objctData2);

    await new_data2.forEach(async key => {
      await this.month_rawPeriods2.push(key.value);
    });
    const objctData3 = { data: this.month_rawPeriods2, label: 'Used Pins' };
    await this.month_lineData.push(objctData3);
    this.lineChartData = await this.month_lineData;
    this.lineChartLabels = await this.getDateArray(this.month_periods);
  }
  setPeriodAndGetChartData(value: string): void {
    if (this.period !== value) {
      this.period = value;
    }

    this.getOrdersChartData(value);
    this.getProfitChartData(value);
  }

  changeTab(selectedTab) {
    if (selectedTab.tabTitle === 'Profit') {
      this.profitChart.resizeChart();
    } else {
      this.ordersChart.resizeChart();
    }
  }

  getOrdersChartData(period: string) {
    this.ordersProfitChartService.getOrdersChartData(period)
      .pipe(takeWhile(() => this.alive))
      .subscribe(ordersChartData => {
        console.log('ordersChartData', ordersChartData);
        this.ordersChartData = ordersChartData;
      });
  }

  getProfitChartData(period: string) {
    this.ordersProfitChartService.getProfitChartData(period)
      .pipe(takeWhile(() => this.alive))
      .subscribe(profitChartData => {
        this.profitChartData = profitChartData;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }
  private getDataForWeekPeriod(b, lineData): OrdersChart {
    return {
      chartLabel: this.getDataLabels(5, b),
      linesData: [
        lineData,
      ],
    };
  }
  getDataLabels(nPoints: number, labelsArray: string[]): string[] {
    const labelsArrayLength = labelsArray.length;
    const step = Math.round(nPoints / labelsArrayLength);

    return Array.from(Array(nPoints)).map((item, index) => {
      const dataIndex = Math.round(index / step);

      return index % step === 0 ? labelsArray[dataIndex] : '';
    });
  }

  hhgetOrdersChartData(period: string): OrdersChart {
    return this.data[period];
  }
  getDateArray(arrayDAta) {
    const new_value = [];
    arrayDAta.forEach(element => {
      new_value.push(this.convertToWord(element));
    });
    console.log("element?????????", new_value);
    return new_value;
  }
  convertToWord(value) {
    if (value) {
      const yrNo = value.split('/')[0];
      const mnthNo = Number(value.split('/')[1]);
      const month = new Array();
      month[0] = 'Jan';
      month[1] = 'Feb';
      month[2] = 'Mar';
      month[3] = 'Apr';
      month[4] = 'May';
      month[5] = 'Jun';
      month[6] = 'Jul';
      month[7] = 'Aug';
      month[8] = 'Sep';
      month[9] = 'Oct';
      month[10] = 'Nov';
      month[11] = 'Dec';
      console.log("month umber", mnthNo);
      if (mnthNo == 1) {
        return month[0] + `/${yrNo}`;
      }
      if (mnthNo == 2) {
        return month[1] + `/${yrNo}`;
      }
      if (mnthNo == 3) {
        return month[2] + `/${yrNo}`;
      }
      if (mnthNo == 4) {
        return month[3] + `/${yrNo}`;
      }
      if (mnthNo == 5) {
        return month[4] + `/${yrNo}`;
      }
      if (mnthNo == 6) {
        return month[5] + `/${yrNo}`;
      }
      if (mnthNo == 7) {
        return month[6] + `/${yrNo}`;
      }
      if (mnthNo == 8) {
        return month[7] + `/${yrNo}`;
      }
      if (mnthNo == 9) {
        return month[8] + `/${yrNo}`;
      }
      if (mnthNo == 10) {
        return month[9] + `/${yrNo}`;
      }
      if (mnthNo == 11) {
        return month[10] + `/${yrNo}`;
      }
      if (mnthNo == 12) {
        return month[11] + `/${yrNo}`;
      }
    }

  }
}
