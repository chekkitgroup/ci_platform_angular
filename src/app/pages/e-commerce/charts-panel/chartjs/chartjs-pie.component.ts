import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Subscription } from 'rxjs';
import { User } from '../../../../_models/user';
import { AuthenticationService, ProductService } from '../../../../_services';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'ngx-chartjs-pie',
  template: `
    <chart type="pie" [data]="data" [options]="options"></chart>
  `,
})
export class ChartjsPieComponent implements OnDestroy {
  data: any;
  options: any;
  themeSubscription: any;
  currentUserSubscription: Subscription;
  currentUser: User;
  public alive = true;

  constructor(private theme: NbThemeService, private authenticationService: AuthenticationService,
    private _productService: ProductService) {
    let loyalty;
    let airtime;
    let merchandize;
    let reward_claimed;
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
      _productService.getUserRewardPieChartAnalytics(this.currentUser.id).pipe(takeWhile(() => this.alive))
        .subscribe((summary) => {
          console.log("get here");
          if (summary['data']) {
            console.log("get here 1");
            if (Number(summary['data'].loyalty)) {
              loyalty = Number(summary['data'].loyalty);
            } else {
              loyalty = 0;
            }
            console.log("get here 2", summary['data']);
            if (summary['data'].rewards) {
              console.log("get here 3");
              if (summary['data'].rewards[0]) {
                airtime = Number(summary['data'].rewards[0].count);
              } else {
                airtime = 0;
              }
              if (summary['data'].rewards[1]) {
                merchandize = Number(summary['data'].rewards[1].count);
              } else {
                merchandize = 0;
              }
            }
          }
          this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
            const colors: any = config.variables;
            const chartjs: any = config.variables.chartjs;
            console.log("datas", loyalty, airtime, merchandize);
            this.data = {
              labels: ['Airtime', 'Merchandize', 'Loyalty Points', 'Rewards Claimed'],
              datasets: [{
                data: [airtime || 0, merchandize || 0, loyalty || 0, reward_claimed || 0],
                backgroundColor: [colors.primaryLight, colors.infoLight, colors.successLight, colors.dangerLight],
              }],
            };

            this.options = {
              maintainAspectRatio: false,
              responsive: true,
              scales: {
                xAxes: [
                  {
                    display: false,
                  },
                ],
                yAxes: [
                  {
                    display: false,
                  },
                ],
              },
              legend: {
                labels: {
                  fontColor: chartjs.textColor,
                },
              },
            };
          });
        });
    });

  }

  ngOnDestroy(): void {
    // if (this.themeSubscription.subscribe()) {
    //   this.themeSubscription.unsubscribe();
    // }
  }
}
