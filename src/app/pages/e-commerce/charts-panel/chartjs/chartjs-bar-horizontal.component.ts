import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { User } from '../../../../_models/user';
import { AuthenticationService, ProductService } from '../../../../_services';
import { takeWhile } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ngx-chartjs-bar-horizontal',
  template: `
    <chart type="horizontalBar" [data]="data" [options]="options"></chart>
  `,
})
export class ChartjsBarHorizontalComponent implements OnDestroy {
  data: any;
  options: any;
  themeSubscription: any;
  currentUserSubscription: Subscription;
  currentUser: User;
  public alive = true;

  constructor(private theme: NbThemeService, private authenticationService: AuthenticationService,
    private _productService: ProductService) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      let Labels = [];
      const productData = [];
      const batchData = [];
      console.log('User value', this.currentUser);
      _productService.getUserproductSubProductAnalytics(this.currentUser.id).pipe(takeWhile(() => this.alive))
        .subscribe((summary) => {
          console.log("taywywyw  >>>>   ", summary);
          let dataObjct = summary['data'];
          dataObjct.forEach(element => {
            Labels.push(element.date);
            productData.push(element.count);
            batchData.push(element.sub_count);
          });
          this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

            const colors: any = config.variables;
            const chartjs: any = config.variables.chartjs;

            this.data = {
              labels: this.getDateArray(Labels),
              datasets: [{
                label: 'Products',
                backgroundColor: colors.infoLight,
                borderWidth: 1,
                data: productData,
              }, {
                label: 'Batches',
                backgroundColor: colors.successLight,
                data: batchData,
              },
              ],
            };

            this.options = {
              responsive: true,
              maintainAspectRatio: false,
              elements: {
                rectangle: {
                  borderWidth: 2,
                },
              },
              scales: {
                xAxes: [
                  {
                    gridLines: {
                      display: true,
                      color: chartjs.axisLineColor,
                    },
                    ticks: {
                      fontColor: chartjs.textColor,
                    },
                  },
                ],
                yAxes: [
                  {
                    gridLines: {
                      display: false,
                      color: chartjs.axisLineColor,
                    },
                    ticks: {
                      fontColor: chartjs.textColor,
                    },
                  },
                ],
              },
              legend: {
                position: 'right',
                labels: {
                  fontColor: chartjs.textColor,
                },
              },
            };
          });
        });
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
  getDateArray(arrayDAta) {
    const new_value = [];
    arrayDAta.forEach(element => {
      new_value.push(this.convertToWord(element));
    });
    return new_value;
  }
  convertToWord(value) {
    if (value) {
      const yrNo = value.split('/')[0];
      const mnthNo = Number(value.split('/')[1]);
      const month = new Array();
      month[0] = 'Jan';
      month[1] = 'Feb';
      month[2] = 'Mar';
      month[3] = 'Apr';
      month[4] = 'May';
      month[5] = 'Jun';
      month[6] = 'Jul';
      month[7] = 'Aug';
      month[8] = 'Sep';
      month[9] = 'Oct';
      month[10] = 'Nov';
      month[11] = 'Dec';
      console.log("month umber", mnthNo);
      if (mnthNo == 1) {
        return month[0] + `/${yrNo}`;
      }
      if (mnthNo == 2) {
        return month[1] + `/${yrNo}`;
      }
      if (mnthNo == 3) {
        return month[2] + `/${yrNo}`;
      }
      if (mnthNo == 4) {
        return month[3] + `/${yrNo}`;
      }
      if (mnthNo == 5) {
        return month[4] + `/${yrNo}`;
      }
      if (mnthNo == 6) {
        return month[5] + `/${yrNo}`;
      }
      if (mnthNo == 7) {
        return month[6] + `/${yrNo}`;
      }
      if (mnthNo == 8) {
        return month[7] + `/${yrNo}`;
      }
      if (mnthNo == 9) {
        return month[8] + `/${yrNo}`;
      }
      if (mnthNo == 10) {
        return month[9] + `/${yrNo}`;
      }
      if (mnthNo == 11) {
        return month[10] + `/${yrNo}`;
      }
      if (mnthNo == 12) {
        return month[11] + `/${yrNo}`;
      }
    }

  }
  private random() {
    return Math.round(Math.random() * 100);
  }
}
