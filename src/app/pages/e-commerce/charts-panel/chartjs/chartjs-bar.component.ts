import { Component, OnDestroy } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
import { User } from '../../../../_models/user';
import { AuthenticationService, ProductService } from '../../../../_services';
import { takeWhile } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ngx-chartjs-bar',
  template: `
    <chart type="bar" [data]="data" [options]="options"></chart>
  `,
})
export class ChartjsBarComponent implements OnDestroy {
  data: any;
  options: any;
  themeSubscription: any;
  currentUserSubscription: Subscription;
  currentUser: User;
  public alive = true;

  constructor(private theme: NbThemeService, private authenticationService: AuthenticationService,
    private _productService: ProductService) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      let Labels;
      const mobileData = [];
      const ussdData = [];
      console.log('User value', this.currentUser);
      _productService.getUserScanHistryAnalytics(this.currentUser.id).pipe(takeWhile(() => this.alive))
        .subscribe((summary) => {
          this.data = summary['data'];
          console.log('Object.keys(this.data)', Object.keys(this.data));
          Labels = this.getDateArray(Object.keys(this.data));
          const objData = Object.values(this.data);
          if (objData.length == 1) {
            const newData = objData[0][0];
            if (newData.ussd) {
              ussdData.push(newData.ussd);
              mobileData.push(0);
            } else if (newData.mobile) {
              mobileData.push(newData.ussd);
              ussdData.push(0);
            }
          } else if (objData.length > 1) {
            objData.forEach(element => {
              mobileData.push(element[0].mobile);
              ussdData.push(element[1].ussd);
            });
          } else {
            mobileData.push(0);
            ussdData.push(0);
          }
          this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
            const colors: any = config.variables;
            const chartjs: any = config.variables.chartjs;

            this.data = {
              labels: Labels,
              datasets: [{
                data: mobileData,
                label: 'Mobile Scan',
                backgroundColor: NbColorHelper.hexToRgbA(colors.primaryLight, 0.8),
              }, {
                data: ussdData,
                label: 'Ussd Scan',
                backgroundColor: NbColorHelper.hexToRgbA(colors.infoLight, 0.8),
              }],
            };

            this.options = {
              maintainAspectRatio: false,
              responsive: true,
              legend: {
                labels: {
                  fontColor: chartjs.textColor,
                },
              },
              scales: {
                xAxes: [
                  {
                    gridLines: {
                      display: false,
                      color: chartjs.axisLineColor,
                    },
                    ticks: {
                      fontColor: chartjs.textColor,
                    },
                  },
                ],
                yAxes: [
                  {
                    gridLines: {
                      display: true,
                      color: chartjs.axisLineColor,
                    },
                    ticks: {
                      fontColor: chartjs.textColor,
                    },
                  },
                ],
              },
            };
          });
        });
    });
  }
  async computeWeekData(new_data) {
    await new_data.forEach(key => {
      // this.week_periods.push(key.week_name);
    });
  }
  getDateArray(arrayDAta) {
    const new_value = [];
    arrayDAta.forEach(element => {
      console.log('element', element);
      new_value.push(this.convertToWord(element));
    });
    return new_value;
  }
  convertToWord(value) {
    if (value) {
      const yrNo = value.split('/')[0];
      const mnthNo = Number(value.split('/')[1]);
      const month = new Array();
      month[0] = 'Jan';
      month[1] = 'Feb';
      month[2] = 'Mar';
      month[3] = 'Apr';
      month[4] = 'May';
      month[5] = 'Jun';
      month[6] = 'Jul';
      month[7] = 'Aug';
      month[8] = 'Sep';
      month[9] = 'Oct';
      month[10] = 'Nov';
      month[11] = 'Dec';
      console.log("month umber", mnthNo);
      if (mnthNo == 1) {
        return month[0] + `/${yrNo}`;
      }
      if (mnthNo == 2) {
        return month[1] + `/${yrNo}`;
      }
      if (mnthNo == 3) {
        return month[2] + `/${yrNo}`;
      }
      if (mnthNo == 4) {
        return month[3] + `/${yrNo}`;
      }
      if (mnthNo == 5) {
        return month[4] + `/${yrNo}`;
      }
      if (mnthNo == 6) {
        return month[5] + `/${yrNo}`;
      }
      if (mnthNo == 7) {
        return month[6] + `/${yrNo}`;
      }
      if (mnthNo == 8) {
        return month[7] + `/${yrNo}`;
      }
      if (mnthNo == 9) {
        return month[8] + `/${yrNo}`;
      }
      if (mnthNo == 10) {
        return month[9] + `/${yrNo}`;
      }
      if (mnthNo == 11) {
        return month[10] + `/${yrNo}`;
      }
      if (mnthNo == 12) {
        return month[11] + `/${yrNo}`;
      }
    }

  }
  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
