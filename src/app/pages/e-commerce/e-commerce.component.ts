import { Component } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-ecommerce',
  styleUrls: ['./e-commerce.component.scss'],
  templateUrl: './e-commerce.component.html',
})
export class ECommerceComponent {
  constructor(private authenticationService: AuthenticationService, private router: Router) {
    if (!authenticationService.tokenExist()) {
      router.navigate(['pages/auth/login']);
    }
  }
}
