import { Component, OnDestroy } from '@angular/core';
import { ProgressInfo, StatsProgressBarData } from '../../../@core/data/stats-progress-bar';
import { takeWhile } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { User } from '../../../_models/user';
import { AuthenticationService } from '../../../_services';
import { ProductService } from '../../../_services/product.service';

@Component({
  selector: 'ngx-progress-section',
  styleUrls: ['./progress-section.component.scss'],
  templateUrl: './progress-section.component.html',
})
export class ECommerceProgressSectionComponent implements OnDestroy {

  private alive = true;

  progressInfoData: ProgressInfo[];
  currentUserSubscription: Subscription;
  currentUser: User;

  constructor(private authenticationService: AuthenticationService, private _productService: ProductService) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
      _productService.getUserSurveyQestRespAnalytics(this.currentUser.id).pipe(takeWhile(() => this.alive))
        .subscribe((summary) => {
          console.log("summary>>>> last", summary);
          this.progressInfoData = summary['data'];
        });
    });
  }

  ngOnDestroy() {
    this.alive = true;
  }
}
