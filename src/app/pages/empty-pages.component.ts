import { Component } from '@angular/core';

@Component({
  selector: 'ngx-empty-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-empty-column-layout>
      <router-outlet></router-outlet>
    </ngx-empty-column-layout>
  `,
})
export class EmptyPagesComponent {
}
