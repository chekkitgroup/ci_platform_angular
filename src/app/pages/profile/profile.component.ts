import { Component, OnInit } from '@angular/core';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthenticationService, UserService, AlertService, ProfileServices } from '../../_services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  loading = false;
  selectedFile: File;
  name: string;
  subtitle: string;
  currentUser: User;
  currentUserSubscription: Subscription;
  users: User[] = [];
  profileForm: FormGroup;
  submitted = false;
  message: any;
  public imagePath;
  imgURL: any;
  // this.authenticationService.currentUser.subscribe(x => this.currentUser = x);

  constructor(
    private router: Router, 
    private authenticationService: AuthenticationService,
    private userService: UserService, 
    private formBuilder: FormBuilder,
    private alertService: AlertService, 
    private profileService: ProfileServices
    ) {
      this.profileForm = this.formBuilder.group({
        company_email: ['', Validators.required],
        company_name: ['', [Validators.required]],
        photo: ['', Validators.required],
        reg_number: ['', Validators.required]
      });
  }

  ngOnInit() {


    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log(this.currentUser);
      this.getProfile();
      // this.profileForm = this.formBuilder.group({
      //   company_email: [this.currentUser.profile.company_email, Validators.required],
      //   company_name: [this.currentUser.profile.company_name, [Validators.required]],
      //   photo: ['', Validators.required],
      //   reg_number: [this.currentUser.profile.reg_number, Validators.required]
      // });
    });
  }
  get f() { return this.profileForm.controls; }

  onFileChanged(event, files) {
    this.selectedFile = event.target.files[0];
    console.log('yeah', files);
    this.profileForm.get('photo').setValue(this.selectedFile);
    this.preview(files);
  }
  preview(files) {
    if (files.length === 0) {
      return;
    }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
  }
  
  getProfile() {
    this.profileService.getProfile(this.currentUser.username)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data)
          let profile = data['data'];
          console.log(profile)
          this.currentUser.profile = profile;
          this.profileForm = this.formBuilder.group({
            company_email: [profile.company_email, Validators.required],
            company_name: [profile.company_name, [Validators.required]],
            photo: [profile.profile_picture_url, Validators.required],
            reg_number: [profile.reg_number, Validators.required]
          });

        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }


  onSubmit() {
    console.log('get here');
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.profileForm.invalid);
    if (this.profileForm.invalid) {
      return;
    }
    this.loading = true;
    console.log(this.profileForm.value);
    const formData = new FormData();
    formData.append('photo', this.profileForm.get('photo').value);
    formData.append('company_email', this.profileForm.get('company_email').value);
    formData.append('company_name', this.profileForm.get('company_name').value);
    formData.append('reg_number', this.profileForm.get('reg_number').value);
    this.profileService.updateProfile(formData)
      .pipe(first())
      .subscribe(
        data => {
          if (data['status']) {
            console.log("new here", data['data']);
            this.alertService.success('Profile updated successful', true);
            this.submitted = false;
            this.loading = false;
            this.message = 'Profile updated successfully';
            let returnedData = data['data'];
            console.log(returnedData)
            // this.getProfile()
            let u = JSON.parse(localStorage.getItem('currentUser'));
            // u.data.user.
            // this.currentUser.profile.profile_picture_url = returnedData.profile_picture_url;
            // this.currentUser.profile.company_name = returnedData.company_name;
            // this.currentUser.profile.company_email = returnedData.company_email;
            // this.currentUser.profile.reg_number = returnedData.reg_number;
            // console.log("new here 1", u);

            // console.log("new here", u);
            u.data.user.company_name = returnedData.company_name;
            u.data.user.company_email = returnedData.company_email;
            u.data.user.reg_number = returnedData.reg_number;
            u.data.user.profile.profile_picture_url = returnedData.profile_picture_url;
            // console.log("new here", u);
   
            localStorage.setItem('currentUser', JSON.stringify(u));
          }
          else {
            this.submitted = false;
            this.loading = false;
            this.message = 'Profile failed to update';
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
