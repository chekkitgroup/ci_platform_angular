import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';
import { SurveyService } from '../../_services/survey.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../../_services/alert.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'ngx-create-survey',
  templateUrl: './create-survey.component.html',
  styleUrls: ['./create-survey.component.scss']
})
export class CreateSurveyComponent implements OnInit {
  surveyForm: FormGroup;
  surveyQuestForm: FormGroup;
  questions: any = [];
  options: string[] = [null, null];
  loading = false;
  submitted = false;
  qust_num = 1;
  success_msg: any;
  constructor(private formBuilder: FormBuilder, private surveyService: SurveyService,
    private alertService: AlertService, private router: Router, private authenticationService: AuthenticationService) {
    // if (!authenticationService.tokenExist()) {
    //   router.navigate(['pages/auth/login']);
    // }
  }

  ngOnInit() {
    this.surveyForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      hasSurvey: ['1', Validators.required],
      question: [null, [Validators.required, Validators.maxLength(200), noWhitespaceValidator]],
      choice1: [null, [Validators.required, Validators.maxLength(200), noWhitespaceValidator]],
      choice2: [null, [Validators.required, Validators.maxLength(200), noWhitespaceValidator]],
    });
  }
  addMoreQuestsion() {
    this.updateQuestion();
    const formData = this.surveyForm.value;
    console.log('this is the question', this.questions);
    this.decreaseOptions();
    this.surveyForm.setValue({
      title: formData.title,
      content: formData.content,
      hasSurvey: formData.hasSurvey,
      question: '',
      choice1: '',
      choice2: ''
    });
    this.qust_num += 1;
  }
  updateQuestion() {
    const formData = this.surveyForm.value;
    const choices: Array<{ text: string }> = [];
    for (let index = 0; index < this.options.length; index++) {
      choices.push({ text: formData[`choice${index + 1}`] });
    }
    const questObject = {
      content: formData.question,
      choices: JSON.stringify(choices)
    };
    this.questions.push(questObject);
  }
  increaseOptions() {
    console.log(`${this.options.length}`);
    if (this.options.length < 3) {
      this.options.push(null);
      this.surveyForm.registerControl(`choice${this.options.length}`,
        new FormControl(null, [Validators.maxLength(20), noWhitespaceValidator]));
    }
  }
  get f() { return this.surveyForm.controls; }
  decreaseOptions() {
    if (this.options.length > 2) {
      console.log(`choice${this.options.length}`);
      this.surveyForm.removeControl(`choice${this.options.length}`);
      this.options.pop();

    }
  }
  createSurvey() {
    this.onSubmit();
  }
  onSubmit() {
    this.updateQuestion();
    let surveyObjct = {
      title: this.surveyForm.value.title,
      content: this.surveyForm.value.content,
      type: this.surveyForm.value.hasSurvey
    };
    this.submitted = true;
    this.loading = true;
    console.log(this.surveyForm.value);
    this.surveyService.addSurvey({ survey: surveyObjct, question: this.questions })
      .pipe(first())
      .subscribe(
        data => {
          console.log("ok>>", data['data']);
          if (data['status']) {
            this.success_msg = "Survey added successfully.";
            this.router.navigate(['/pages/survey']);
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
