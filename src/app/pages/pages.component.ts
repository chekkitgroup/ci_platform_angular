import { Component } from '@angular/core';
import { AuthenticationService, UserService } from './../_services';

import { MENU_ITEMS, MENU_ITEMS1,MENU_ITEMS2 } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="accessLevel == 1?menu1:(accessLevel == 2?menu2:menu) "></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {
  currentUser: any;
  accessLevel: any;
  menuItems: any;

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService)
     {

      this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user.data.user;
        this.accessLevel = this.currentUser.access_level;
        console.log(this.accessLevel);
      });
     }

     menu = MENU_ITEMS;
     menu1 = MENU_ITEMS1;
     menu2 = MENU_ITEMS2;
}
