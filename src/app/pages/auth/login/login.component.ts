import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService, AlertService } from '../../../_services';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  err_msg: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
  ) {
    if (authenticationService.tokenExist()) {
      router.navigate(['pages/overview']);
    } else {
      if (this.authenticationService.currentUserValue) {
        this.router.navigate(['/']);
      }
      this.authenticationService.logout();
    }
    // redirect to home if already logged in

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    console.log('>>>>returnUrl', this.returnUrl);
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log("yes", data);
          this.router.navigate(['pages/overview']);
        },
        error => {
          console.log('data>>>', error);
          this.err_msg = error;
          this.alertService.error(error);
          this.loading = false;
          this.submitted = false;
        });
  }
  onLoggedin() {
    localStorage.setItem('isLoggedin', 'true');
  }

}
