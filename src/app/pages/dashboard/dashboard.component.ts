import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService, NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';
import { SolarData } from '../../@core/data/solar';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { AuthenticationService, UserService } from '../../_services';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import {UtilityProvider} from "../../_helpers/utility";

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnDestroy, OnInit {

  private alive = true;
  solarValue: number;
  userSub: any;
  statusCards: string;
  commonStatusCardsSet: CardSettings[] = [
  ];
  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    dark: CardSettings[];
  } = {
      default: this.commonStatusCardsSet,
      cosmic: this.commonStatusCardsSet,
      dark: this.commonStatusCardsSet,
    };
  index = 1;
  subtitle: string;
  currentUser: User;
  currentUserSubscription: Subscription;
  destroyByClick = true;
  duration = 5000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';

  title = 'HI there!';
  content = `payment successful!`;

  types: NbComponentStatus[] = [
    'primary',
    'success',
    'info',
    'warning',
    'danger',
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];
  constructor(
    private themeService: NbThemeService, 
    private router: Router, 
    private toastrService: NbToastrService,
    private solarService: SolarData, 
    public utility: UtilityProvider,
    private authenticationService: AuthenticationService,
    private userService: UserService
    ) {
    if (!authenticationService.tokenExist()) {
      router.navigate(['pages/auth/login']);
    }
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log(this.currentUser);
      this.getUserSubscription();
      this.themeService.getJsTheme()
        .pipe(takeWhile(() => this.alive))
        .subscribe(theme => {
          this.statusCards = this.statusCardsByThemes[theme.name];
        });

      this.solarService.getSolarData()
        .pipe(takeWhile(() => this.alive))
        .subscribe((data) => {
          this.solarValue = data;
        });
    });
  }
  ngOnInit() {

  }
  ngOnDestroy() {
    this.alive = false;
  }
  async getUserSubscription() {
    await this.userService.getUserSubDetails(this.currentUser.id)
      .pipe(first())
      .subscribe(
        data => {
          console.log('data subscription', data);
          if (data['status']) {
            this.userSub = data['data'];
            if (this.userSub.all_counts.account_balance < 1) {
              this.showToast(this.status, this.title, 'You need to make payment for scanning and taking surveys on mobile and ussd channels');
            } else if (this.userSub.all_counts.gen_pins_balance > 0) {
              this.showToast(this.status, this.title, 'You need to make payment for the PINS generated to activate your CHANNELS');
            } else if (this.userSub.all_counts.rewards_balance < 1) {
              this.showToast(this.status, this.title, 'You need to make payment for airtime reward disbursement');
            }
          }
        },
        error => {
        });
  }
  PinsPayment(amount) {
    const data = {
      email: this.currentUser.email,
      amount: amount,
      type: 'pin',
    };
    this.router.navigate(['/pages/payments'], { queryParams: data, skipLocationChange: true });
  }
  accountTopup() {
    const data = {
      email: this.currentUser.email,
      amount: 0,
      type: 'account',
    };
    this.router.navigate(['/pages/payments'], { queryParams: data, skipLocationChange: true });
  }
  AirtimeTopup() {
    const data = {
      email: this.currentUser.email,
      amount: 0,
      type: 'airtime',
    };
    this.router.navigate(['/pages/payments'], { queryParams: data, skipLocationChange: true });
  }
  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
}
