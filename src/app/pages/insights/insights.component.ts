import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService, SurveyQuestService, SurveyService, AlertService, SurveyRewardService, ProductService, RestService } from '../../_services';
import { first } from 'rxjs/operators';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { UserService } from '../../_services/user.service';
import { Chart } from '../../../../node_modules/chart.js'
import { NbDialogService } from '@nebular/theme';
import { AddRewardComponent } from '../../_shared/modals/add-reward/add-reward.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as c3 from 'c3';


import * as moment from 'moment';
declare var google;


@Component({ 
  selector: 'ngx-insights',
  templateUrl: './insights.component.html',
  styleUrls: ['./insights.component.scss']
})




export class InsightsComponent implements OnInit {

  title = 'dashboard';
  chart;
  chart2 = [];
  pie: any;
  doughnut: any;
  callData: any;
  giftData: any;
  rows: any;
  data: any;
  temp: any[];
  scanStatistics;

  // editing = {};
  // // rows = [];
  // temp = [];
  // success_msg: any;
  // loadingIndicator = true;
  // reorderable = true;
  // closeResult: any;
  // surveyForm: FormGroup;
  // loading = false;
  // submitted = false;
  // save_survey: any;
  // save_products: any;
  // selectChange: any;
  // currentUser: User;
  // currentUserSubscription: Subscription;
  // user_recordCounts: any;
  // products: any;
  // myDate = new Date();
  // aWeekBefore = new Date(this.myDate.getTime() - 60 * 60 * 24 * 7 * 1000);
  // aMonthTime = new Date(this.myDate.getTime() + 60 * 60 * 24 * 30 * 1000);

  @ViewChild('map', { static: true }) mapElement: ElementRef;
  map: any;
  ageDistribution: any;
  genderDistribution: any;
  rows2: any;
  row;
  submitting;

  // public filterOptions = {
  //   startDate : moment(this.aWeekBefore).format('YYYY-MM-DD'),
  //   endDate : moment(this.myDate).format('YYYY-MM-DD'),
  //   productId : "",
  // };

  // // lineChart
  // public lineChartData: Array<any> = [
  // ];
  // public lineChartLabels: Array<any> = [];
  // public lineChartOptions: any = {
  //   responsive: true
  // };
  // public lineChartColors: Array<any> = [
  //   {
  //     // grey
  //     backgroundColor: 'rgba(54,190,166,.1)',
  //     borderColor: '#36bea6',
  //     pointBackgroundColor: '#36bea6',
  //     pointBorderColor: '#fff',
  //     pointHoverBackgroundColor: '#fff',
  //     pointHoverBorderColor: '#36bea6'
  //   },
  //   {
  //     // dark grey
  //     backgroundColor: 'rgb(41,98,255,.1)',
  //     borderColor: '#2962FF',
  //     pointBackgroundColor: '#2962FF',
  //     pointBorderColor: '#fff',
  //     pointHoverBackgroundColor: '#fff',
  //     pointHoverBorderColor: '#2962FF'
  //   }
  // ];
  // public lineChartLegend = true;
  // public lineChartType = 'line';
  // scanStatistics: any;
  // scanCords = [];
  // heatmap: any;



  constructor(
    private productService: ProductService, 
    private modalService: NgbModal,
    private formBuilder: FormBuilder, 
    private router: Router,
    private _rest: RestService, 
    private authenticationService: AuthenticationService, 
    private surveyService: SurveyService,
    private userService: UserService) {

    // this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
    //   this.currentUser = user.data.user;
    //   console.log('User value', this.currentUser);
    // });
    // this.getAllSurvey();
    // this.getAllProducts();
    // setTimeout(() => { this.loadingIndicator = false; }, 1500);
  }
  ngOnInit() {

    this.getVoiceCallLeads();
    this.getGifts();
    this.getDistribution();
    this.getWinners();
    /// doughnut
  // this.doughnut =  new Chart('doughnut',{
  //   type: 'doughnut',
  //   options: {
  //     responsive: true,
  //     title: {
  //       display: true,
  //       text: 'Doughnut Chart'
  //     },legend: {
  //       position: 'top',
  //     },animation: {
  //       animateScale: true,
  //       animateRotate: true
  //     }
  //   },
  //   data: {
  //     datasets: [{
  //       data: [45,10,5,25,15],
  //       backgroundColor: ["red","orange","yellow","green","blue"],
  //       label: 'Dataset 1'
  //     }],
  //     labels: ['Red','Orange','Yellow','Green','Blue']
  //   }
  // })

  



    // this.initMap()
  }

//   ngAfterViewInit() {
//     let chart = c3.generate({
//     bindto: '#chart',
//         data: {
//             columns: [
//                 ['data1', 30, 200, 100, 400, 150, 250],
//                 ['data2', 50, 20, 10, 40, 15, 25]
//             ]
//         }
//     });
// }

  openAddRewardModal() {
    const modalRef = this.modalService.open(AddRewardComponent, {size: 'lg', centered: true, windowClass: 'clear-bg-modal' })
  }

  // initMap(){
  //   this.getPosition().then(pos=>
  //     {
  //       //  console.log(`Positon: ${pos.lng} ${pos.lat}`);
  //       var mapProp = {
  //         center: new google.maps.LatLng(pos.lat,pos.lng),
  //         zoom: 10,
  //         mapTypeId: google.maps.MapTypeId.ROADMAP,
  //         disableDefaultUI: true,
  //         zoomControl: true,
  //         scaleControl: false,
  //       };
  //       // this.map = new google.maps.Map(this.map1, mapProp);
  //       this.map = new google.maps.Map(this.mapElement.nativeElement, mapProp);
  //     });
  // }


  

  gotoPrintCode(){

  }


 getVoiceCallLeads() {
  this._rest.getVoiceCallLeads().pipe(first()).subscribe(data => {
    // this.scanStatistics = data['data'];
    // console.log(data['data']);
    this.callData = data['data'];
    
  });
}

getGifts() {
  this._rest.getGifts().pipe(first()).subscribe(data => {
    // this.scanStatistics = data['data'];
    // console.log(data['data']);
    this.rows = data['data'];
  });
}

getWinners() {
  this._rest.getWinners().pipe(first()).subscribe(data => {
    // this.scanStatistics = data['data'];
    // console.log(data['data']);
    this.rows2 = data['data'];
  });
}


getDistribution() {
  this._rest.getDistribution().pipe(first()).subscribe(data => {
    this.ageDistribution = data['data'].age;
    this.genderDistribution = data['data'].gender;

    this.ageDistribution.labels = [];
    this.ageDistribution.values = [];

    this.genderDistribution.labels = [];
    this.genderDistribution.values = [];

    for (let i = 0; i < this.ageDistribution.length; i++) {
      if(this.ageDistribution[i].age){
        this.ageDistribution.labels.push(this.ageDistribution[i].age)
      }
      if(this.ageDistribution[i].ageCount){
      this.ageDistribution.values.push(this.ageDistribution[i].ageCount)
      }
      console.log(this.ageDistribution[i]);
    }
    
    for (let i = 0; i < this.genderDistribution.length; i++) {
      if(this.genderDistribution[i].gender && this.genderDistribution[i].genderCount){
        this.genderDistribution.labels.push(this.genderDistribution[i].gender)      
        this.genderDistribution.values.push(this.genderDistribution[i].genderCount)
      }
    }
    console.log(this.genderDistribution);
  
  });
}




}

