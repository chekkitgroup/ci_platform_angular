import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLoyaltyPointComponent } from './view-loyalty-point.component';

describe('ViewLoyaltyPointComponent', () => {
  let component: ViewLoyaltyPointComponent;
  let fixture: ComponentFixture<ViewLoyaltyPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLoyaltyPointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLoyaltyPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
