import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductPinsComponent } from './product-pins.component';

describe('ProductPinsComponent', () => {
  let component: ProductPinsComponent;
  let fixture: ComponentFixture<ProductPinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductPinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
