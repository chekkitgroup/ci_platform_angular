import { Component, OnInit } from '@angular/core';
import { ProductService, AuthenticationService } from '../../_services';
import { Router } from '@angular/router';
import { first, filter } from 'rxjs/operators';
import { User } from '../../_models/user';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ngx-generate-pin',
  templateUrl: './generate-pin.component.html',
  styleUrls: ['./generate-pin.component.scss']
})
export class GeneratePinComponent implements OnInit {
  currentUserSubscription: Subscription;
  currentUser: User;
  save_products: any;
  newPinForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  productObjct: any;
  batchObjct: any;
  save_sub_products: any;

  constructor(private productService: ProductService,
    private authenticationService: AuthenticationService, private router: Router, private formBuilder: FormBuilder) {
      
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
    });
    this.getAllProducts();
  }

  ngOnInit() {
    this.newPinForm = this.formBuilder.group({
      pin_quantity: ['', Validators.required],
      pin_type: ['', Validators.required],
      product_id: ['', Validators.required],
      sub_product_id: ['', Validators.required],
    });
  }
  getAllProducts() {
    this.productService.getAllUserProducts(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log(data['data'].products);
      if (data['data'].products) {
        this.save_products = data['data'].products;
      }
    });
  }
  onChange(evnt: any) {
    console.log(this.newPinForm.get('product_id').value, 'yeah', evnt.target.value);
    this.get_Allbatches(evnt.target.value);
  }
  get_Allbatches(productId) {
    this.productService.getAllProductBatches(productId).pipe(first()).subscribe(data => {
      if (data['status']) {
        console.log(data['data'].productBatches);
        this.save_sub_products = data['data'].productBatches;
      }
    });
  }
  onSubmit() {
    console.log('get here');
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.newPinForm.invalid);
    if (this.newPinForm.invalid) {
      return;
    }
    this.loading = true;
    console.log(this.newPinForm.value);
    const formData = new FormData();
    formData.append('pin_quantity', this.newPinForm.get('pin_quantity').value);
    formData.append('pin_type', this.newPinForm.get('pin_type').value);
    const sub_productId = this.newPinForm.get('sub_product_id').value;
    this.productService.generate_pin(sub_productId, this.newPinForm.value)
      .pipe(first())
      .subscribe(
        // tslint:disable-next-line: no-shadowed-variable
        dataObjct => {
          console.log('ok>>', dataObjct);
          if (dataObjct['status']) {
            this.batchObjct = this.getBatch(sub_productId);
            this.productObjct = this.getProduct();
            const data = JSON.stringify({
              batch: this.batchObjct,
              product: this.productObjct
            });
            console.log("to send", data);
            this.router.navigate(['/pages/product-pins'], { queryParams: { data }, skipLocationChange: true });
          }
        },
        error => {
          this.loading = false;
        });
  }
  getBatch(sub_productId) {
    return this.save_sub_products.find(batches => Number(batches.id) === Number(sub_productId));
  }
  getProduct() {
    return this.save_products.find(product => Number(product.id) === Number(this.newPinForm.get('product_id').value));
  }
}
