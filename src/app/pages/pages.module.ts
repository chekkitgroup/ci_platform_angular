import { NgModule } from '@angular/core';
import {
  NbMenuModule, NbCardModule, NbUserModule, NbButtonModule, NbTabsetModule,
  NbActionsModule, NbRadioModule, NbSelectModule, NbListModule, NbIconModule,
  NbStepperModule, NbCheckboxModule, NbDatepickerModule, NbInputModule, NbDialogModule, NbWindowModule, NbPopoverModule, NbTooltipModule, NbSpinnerModule, NbBadgeModule,
} from '@nebular/theme';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MomentModule } from 'ngx-moment';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { LoginComponent } from './auth/login/login.component';
import { EmptyPagesComponent } from './empty-pages.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import { ProductComponent } from './product/product.component';
import { UserFeedbackComponent } from './user-feedback/user-feedback.component';
import { SurveyComponent } from './survey/survey.component';
import { RewardsComponent } from './rewards/rewards.component';
import { CodeCollectionComponent } from './code-collection/code-collection.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { RouterModule } from '@angular/router';
import { AddProductComponent } from './add-product/add-product.component';
import { FormsModule as ngFormsModule } from '@angular/forms';
import { ProductBatchesComponent } from './product-batches/product-batches.component';
import { CreateBatchComponent } from './create-batch/create-batch.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DatepickerComponent, DatepickerRenderComponent } from './product-batches/datepicker/datepicker.component';
import { CreateSurveyComponent } from './create-survey/create-survey.component';
import { ViewSurveyComponent } from './view-survey/view-survey.component';
import { EditSurveyComponent } from './edit-survey/edit-survey.component';
import { MerchandizeRewardComponent } from './merchandize-reward/merchandize-reward.component';
import { AirtimeRewardComponent } from './airtime-reward/airtime-reward.component';
import { LoyaltyPointsComponent } from './loyalty-points/loyalty-points.component';
import { RaffleDrawComponent } from './raffle-draw/raffle-draw.component';
import { CreateRedemptionPointComponent } from './create-redemption-point/create-redemption-point.component';
import { AddLocationModal } from './create-redemption-point/create-redemption-point.component';
import { AddLoyaltyPointComponent } from './add-loyalty-point/add-loyalty-point.component';
import { ViewLoyaltyPointComponent } from './view-loyalty-point/view-loyalty-point.component';
import { ProductPinsComponent } from './product-pins/product-pins.component';
import { NewPinComponent } from './new-pin/new-pin.component';
import { ViewBatchComponent } from './view-batch/view-batch.component';
import { GeneratePinComponent } from './generate-pin/generate-pin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { EditBatchComponent } from './edit-batch/edit-batch.component';
import { AllRewardsComponent } from './all-rewards/all-rewards.component';
import { ViewRewardComponent } from './view-reward/view-reward.component';
import { EditAirtimeComponent } from './edit-airtime/edit-airtime.component';
import { EditMerchandizeComponent } from './edit-merchandize/edit-merchandize.component';
import { PaymentsComponent } from './payments/payments.component';
import { Angular4PaystackModule } from 'angular4-paystack';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartsModule } from 'ng2-charts';
import { ChartModule } from 'angular2-chartjs';
import { AnalyticsComponent } from './analytics/analytics.component';
import { ViewRaffleComponent } from './view-raffle/view-raffle.component';
import { AgmCoreModule } from '@agm/core';
import { RetargetingComponent } from './retargeting/retargeting.component';
import { ProgressComponent } from './../_helpers/progress/progress.component';
import { InsightsComponent } from './insights/insights.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AddRewardComponent } from './../_shared/modals/add-reward/add-reward.component';
import { ViewContactsComponent } from './view-contacts/view-contacts.component';




@NgModule({
  imports: [
    
    FormsModule,
    FontAwesomeModule,
    NgbModule,
    ThemeModule,
    NgxChartsModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    ChartsModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NbStepperModule,
    NbCheckboxModule,
    NgxEchartsModule,
    ChartModule,
    RouterModule,
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    FormsModule,    // added here too
    ReactiveFormsModule, // added here too\
    NbInputModule,
    NbDatepickerModule,
    ngFormsModule,
    Ng2SmartTableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AgmCoreModule,
    NgxDatatableModule,
    NbBadgeModule,
    MomentModule,
    NbDialogModule.forChild(),
    NbWindowModule.forChild(),
    NbPopoverModule,
    NbTooltipModule,
    NbSpinnerModule,
    Angular4PaystackModule.forRoot('pk_test_f1e9a210ad5f0309823abab6e037c5eb7424045d'),
  ],
  declarations: [
    EmptyPagesComponent,
    ProgressComponent,
    PagesComponent,
    LoginComponent,
    ProfileComponent,
    ProductComponent,
    UserFeedbackComponent,
    SurveyComponent,
    AddRewardComponent,
    RewardsComponent,
    CodeCollectionComponent,
    AddProductComponent,
    ProductBatchesComponent,
    CreateBatchComponent,
    EditProductComponent,
    DatepickerComponent,
    DatepickerComponent,
    DatepickerRenderComponent,
    CreateSurveyComponent,
    ViewSurveyComponent,
    EditSurveyComponent,
    MerchandizeRewardComponent,
    AirtimeRewardComponent,
    LoyaltyPointsComponent,
    RaffleDrawComponent,
    CreateRedemptionPointComponent,
    AddLocationModal,
    AddLoyaltyPointComponent,
    ViewLoyaltyPointComponent,
    ProductPinsComponent,
    NewPinComponent,
    ViewBatchComponent,
    GeneratePinComponent,
    SignupComponent,
    EditBatchComponent,
    AllRewardsComponent,
    ViewRewardComponent,
    EditAirtimeComponent,
    EditMerchandizeComponent,
    PaymentsComponent,
    AnalyticsComponent,
    ViewRaffleComponent,
    RetargetingComponent,
    ViewContactsComponent,
    InsightsComponent,
  ],
  entryComponents: [
    DatepickerComponent,
    AddLocationModal,
    AddRewardComponent,
    DatepickerRenderComponent,
  ],
})
export class PagesModule {
}
