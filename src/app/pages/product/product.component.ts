import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { User } from '../../_models/user';
import { Subscription } from 'rxjs';
import { ProductService, AuthenticationService } from '../../_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Product } from '../../_models';

@Component({
  selector: 'ngx-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  imageUrl: any;
  editing = {};
  rows = [];
  temp = [];
  success_msg: any;
  loadingIndicator = true;
  reorderable = true;
  closeResult: any;
  data: any;
  productObjct: any;
  productForm: FormGroup;
  pinForm: FormGroup;
  form: FormGroup;
  rewardForm: FormGroup;
  loading = false;
  submitted = false;
  save_products: any;
  save_surveyReward: any;
  selectChange: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  constructor(private productService: ProductService,
    private authenticationService: AuthenticationService, private router: Router) {

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
    });
    this.getAllProducts();
    setTimeout(() => { this.loadingIndicator = false; }, 1500);
  }
  getAllProducts() {
    this.authenticationService.allProducts.subscribe(save_products => {
      if (save_products) {
        this.save_products = save_products;
        this.productService.getAllUserProducts(this.currentUser.id).pipe(first()).subscribe(data => {
          console.log('to save ', data['data'].products);
          if (data['data'].products) {
            this.save_products = data['data'].products;
            localStorage.setItem('allProducts', JSON.stringify(data['data'].products));
          }
        });
      } else {
        this.productService.getAllUserProducts(this.currentUser.id).pipe(first()).subscribe(data => {
          console.log(data['data'].products);
          if (data['data'].products) {
            this.save_products = data['data'].products;
            localStorage.setItem('allProducts', JSON.stringify(data['data'].products));
          }
        });
      }
    });
  }
  ngOnInit() {

  }
  editProduct(product: Product) {
    console.log("product", product);
    let data = JSON.stringify(product);
    this.router.navigate(['/pages/edit-product'], { queryParams: { data }});
  }
  viewPerfoormance(product: Product) {
    console.log("product", product);
    let data = JSON.stringify(product);
    this.router.navigate(['/pages/edit-product'], { queryParams: { data }, skipLocationChange: true });
  }
  ViewProduct(product: Product) {
    console.log("product", product);
    let data = JSON.stringify(product);
    this.router.navigate(['/pages/product-batches'], { queryParams: { data } });
  }
  CreateBatch(product: Product) {
    let data = JSON.stringify(product);
    this.router.navigate(['/pages/create-batch'], { queryParams: { data } });
  }
}

