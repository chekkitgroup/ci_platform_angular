import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRedemptionPointComponent } from './create-redemption-point.component';

describe('CreateRedemptionPointComponent', () => {
  let component: CreateRedemptionPointComponent;
  let fixture: ComponentFixture<CreateRedemptionPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRedemptionPointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRedemptionPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
