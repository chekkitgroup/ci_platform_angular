import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { LoginComponent } from './auth/login/login.component';
import { EmptyPagesComponent } from './empty-pages.component';
import { ProductComponent } from './product/product.component';
import { SurveyComponent } from './survey/survey.component';
import { RewardsComponent } from './rewards/rewards.component';
import { CodeCollectionComponent } from './code-collection/code-collection.component';
import { UserFeedbackComponent } from './user-feedback/user-feedback.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductBatchesComponent } from './product-batches/product-batches.component';
import { CreateBatchComponent } from './create-batch/create-batch.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { CreateSurveyComponent } from './create-survey/create-survey.component';
import { ViewSurveyComponent } from './view-survey/view-survey.component';
import { EditSurveyComponent } from './edit-survey/edit-survey.component';
import { MerchandizeRewardComponent } from './merchandize-reward/merchandize-reward.component';
import { AirtimeRewardComponent } from './airtime-reward/airtime-reward.component';
import { LoyaltyPointsComponent } from './loyalty-points/loyalty-points.component';
import { RaffleDrawComponent } from './raffle-draw/raffle-draw.component';
import { CreateRedemptionPointComponent } from './create-redemption-point/create-redemption-point.component';
import { AddLoyaltyPointComponent } from './add-loyalty-point/add-loyalty-point.component';
import { ProductPinsComponent } from './product-pins/product-pins.component';
import { NewPinComponent } from './new-pin/new-pin.component';
import { ViewBatchComponent } from './view-batch/view-batch.component';
import { GeneratePinComponent } from './generate-pin/generate-pin.component';
import { ProfileComponent } from './profile/profile.component';
import { SignupComponent } from './auth/signup/signup.component';
import { ViewLoyaltyPointComponent } from './view-loyalty-point/view-loyalty-point.component';
import { EditBatchComponent } from './edit-batch/edit-batch.component';
import { AuthGuard } from '../_helpers';
import { AllRewardsComponent } from './all-rewards/all-rewards.component';
import { ViewRewardComponent } from './view-reward/view-reward.component';
import { EditMerchandizeComponent } from './edit-merchandize/edit-merchandize.component';
import { EditAirtimeComponent } from './edit-airtime/edit-airtime.component';
import { PaymentsComponent } from './payments/payments.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { ViewRaffleComponent } from './view-raffle/view-raffle.component';
import { RetargetingComponent } from './retargeting/retargeting.component';
import { ViewContactsComponent } from './view-contacts/view-contacts.component';

import { InsightsComponent } from './insights/insights.component';
// RetargetingComponent
const routes: Routes = [
  {
    path: 'auth',
    component: EmptyPagesComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'signup',
        component: SignupComponent,
      },
      {
        path: '**',
        component: LoginComponent,
      },
    ],
  },
  {
    path: '',
    component: PagesComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: 'overview',
        component: DashboardComponent,
      },
      {
        path: 'dashboard',
        component: ECommerceComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'insights',
        component: InsightsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'analytics',
        component: AnalyticsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'product',
        component: ProductComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'add-product',
        component: AddProductComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'edit-product',
        component: EditProductComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'product-batches',
        component: ProductBatchesComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'product-pins',
        component: ProductPinsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'generate-pin',
        component: GeneratePinComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'new-pin',
        component: NewPinComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'view-batch',
        component: ViewBatchComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'create-batch',
        component: CreateBatchComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'survey',
        component: SurveyComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'create-survey',
        component: CreateSurveyComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'view-survey',
        component: ViewSurveyComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'edit-survey',
        component: EditSurveyComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'edit-batch',
        component: EditBatchComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'rewards',
        component: RewardsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'merchandize-reward',
        component: MerchandizeRewardComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'airtime-reward',
        component: AirtimeRewardComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'all-rewards',
        component: AllRewardsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'view-reward',
        component: ViewRewardComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'edit-merchandize',
        component: EditMerchandizeComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'edit-airtime',
        component: EditAirtimeComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'loyalty-points',
        component: LoyaltyPointsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'add-loyalty-point',
        component: AddLoyaltyPointComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'view-loyalty-point',
        component: ViewLoyaltyPointComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'raffle-draw',
        component: RaffleDrawComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'view-raffle',
        component: ViewRaffleComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'create-redemption-point',
        component: CreateRedemptionPointComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'code-collection',
        component: CodeCollectionComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'user-feedback',
        component: UserFeedbackComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'payments',
        component: PaymentsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'retargeting',
        component: RetargetingComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'retargeting/contacts',
        component: ViewContactsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: '**',
        component: NotFoundComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
