import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../_services/product.service';
import { AuthenticationService, SurveyService, AlertService } from '../../_services';
import { first } from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { SurveyRewardService } from '../../_services/surveyReward.service';

@Component({
  selector: 'ngx-edit-merchandize',
  templateUrl: './edit-merchandize.component.html',
  styleUrls: ['./edit-merchandize.component.scss']
})
export class EditMerchandizeComponent implements OnInit {
  merchantObjct: any;
  imageUrl: any;
  success_msg: any;
  surveyMerchantForm: FormGroup;
  loading = false;
  submitted = false;
  save_products: any;
  save_surveyReward: any;
  save_survey: any;
  selectChange: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  constructor(private productService: ProductService, private formBuilder: FormBuilder, private router: Router,
    private authenticationService: AuthenticationService, private surveyService: SurveyService,
    private activatedRoute: ActivatedRoute, private alertService: AlertService,
    private surveyrewardServices: SurveyRewardService) {
    let dataIs = JSON.parse(this.activatedRoute.snapshot.queryParams.data);
    console.log("dataIs", dataIs);
    this.merchantObjct = dataIs.reward;
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
    });
  }

  ngOnInit() {
    this.surveyMerchantForm = this.formBuilder.group({
      reward_value: [this.merchantObjct.reward_value, Validators.required],
      reward_quant: [this.merchantObjct.reward_quantity, Validators.required],
      reward_type: ['Merchandize', Validators.required],
      reward_point: [this.merchantObjct.point_to_claim_reward, Validators.required],
    });
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.surveyMerchantForm.get('photo').setValue(file);
    }
  }
  get f() { return this.surveyMerchantForm.controls; }

  onSubmit() {
    console.log('get here');
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.surveyMerchantForm.invalid);
    if (this.surveyMerchantForm.invalid) {
      return;
    }
    this.loading = true;
    console.log(this.surveyMerchantForm.value);
    const formData = new FormData();
    // formData.append('photo', this.surveyMerchantForm.get('photo').value);
    formData.append('reward_value', this.surveyMerchantForm.get('reward_value').value);
    formData.append('reward_type', this.surveyMerchantForm.get('reward_type').value);
    formData.append('reward_quant', this.surveyMerchantForm.get('reward_quant').value);
    formData.append('reward_point', this.surveyMerchantForm.get('reward_point').value);

    this.surveyrewardServices.updateMerchatReward(this.merchantObjct.id, this.surveyMerchantForm.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log('ok>>', data['data']);
          if (data['status']) {
            console.log('oka 3 >>', data['data']);
            // this.data = this.save_products;
            this.router.navigate(['/pages/all-rewards']);
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
