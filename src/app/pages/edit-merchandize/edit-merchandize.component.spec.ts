import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMerchandizeComponent } from './edit-merchandize.component';

describe('EditMerchandizeComponent', () => {
  let component: EditMerchandizeComponent;
  let fixture: ComponentFixture<EditMerchandizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMerchandizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMerchandizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
