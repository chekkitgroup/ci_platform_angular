import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../../_services/authentication.service';
import { User } from '../../_models/user';

@Component({
  selector: 'ngx-view-batch',
  templateUrl: './view-batch.component.html',
  styleUrls: ['./view-batch.component.scss'],
})
export class ViewBatchComponent implements OnInit {
  batchObjct: any;
  productObjct: any;
  navData: any;
  currentUserSubscription: Subscription;
  currentUser: User;
  constructor(private activatedRoute: ActivatedRoute, private authenticationService: AuthenticationService,
    private router: Router) {
    this.navData = this.activatedRoute.snapshot.queryParams;
    console.log('product', JSON.parse(this.navData.data));
    this.batchObjct = JSON.parse(this.navData.data).batch;
    this.productObjct = JSON.parse(this.navData.data).product;
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
    });
  }

  ngOnInit() {
  }
  getShelfLife(prod_date: any, exp_date) {
    const diff = (new Date(exp_date)).valueOf() - (new Date(prod_date)).valueOf();
    console.log('result date ', Math.ceil(diff / (1000 * 3600 * 24)));
    return Math.ceil(diff / (1000 * 3600 * 24));
  }
  gotoBatchPins() {
    const data = JSON.stringify({
      batch: this.batchObjct,
      product: this.productObjct,
    });
    this.router.navigate(['/pages/product-pins'], { queryParams: { data, skipLocationChange: true } });
  }
  gotoCreateBatch(batchObjct: any) {
    let data = JSON.stringify(this.productObjct);
    this.router.navigate(['/pages/create-batch'], { queryParams: { data }, skipLocationChange: true });
  }
}
