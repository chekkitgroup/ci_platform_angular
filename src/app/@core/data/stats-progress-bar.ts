import { Observable } from 'rxjs';

export interface ProgressInfo {
  title: string;
  value: number;
}

export abstract class StatsProgressBarData {
  abstract getProgressInfoData(): Observable<ProgressInfo[]>;
}
