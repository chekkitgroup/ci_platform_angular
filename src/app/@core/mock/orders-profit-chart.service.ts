import { of as observableOf, Observable, Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { OrdersChart, OrdersChartData } from '../data/orders-chart';
import { OrderProfitChartSummary, OrdersProfitChartData } from '../data/orders-profit-chart';
import { ProfitChart, ProfitChartData } from '../data/profit-chart';
import { UserService } from '../../_services/user.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { User } from '../../_models/user';
import { first } from 'rxjs/operators';

@Injectable()
export class OrdersProfitChartService extends OrdersProfitChartData {
  currentUser: User;
  currentUserSubscription: Subscription;
  user_recordCounts: any;
  private summary = [
    {
      title: 'Code Generated',
      value: 0,
    },
    {
      title: 'Total Authentications',
      value: 0,
    },
    {
      title: 'User Feedback',
      value: 0,
    },
    {
      title: 'Total Airtime Distributed',
      value: 0,
    },
  ];

  constructor(private ordersChartService: OrdersChartData, private userService: UserService,
    private profitChartService: ProfitChartData, private authenticationService: AuthenticationService) {
    super();
    this.getUserRecordCount();
  }
  async getUserRecordCount() {
    this.authenticationService.recordCounts.subscribe(all_counts => {
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user.data.user;
        if (all_counts) {
          console.log('all counts>>>>>', all_counts);
          this.user_recordCounts = all_counts;
          this.summary = [
            {
              title: 'Code Generated',
              value: this.user_recordCounts.product_pins_count || 0,
            },
            {
              title: 'Total Authentications',
              value: this.user_recordCounts.response_count || 0,
            },
            {
              title: 'User Feedback',
              value: 0,
            },
            {
              title: 'Total Airtime Distributed',
              value: this.user_recordCounts.airtime_distributed || 0,
            },
          ];
          this.userService.getUserRecordCount(this.currentUser.id).pipe(first()).subscribe(async all_counts => {
            console.log('all counts111', all_counts);
            if (all_counts['status']) {
              this.user_recordCounts = await all_counts['data'];
              localStorage.setItem('recordCounts', JSON.stringify(this.user_recordCounts));
            }
          });
        }
        else {
          this.userService.getUserRecordCount(this.currentUser.id).pipe(first()).subscribe(async all_counts => {
            console.log('all counts', all_counts);
            if (all_counts['status']) {
              this.user_recordCounts = await all_counts['data'];
              localStorage.setItem('recordCounts', JSON.stringify(this.user_recordCounts));
              this.summary = [
                {
                  title: 'Code Generated',
                  value: this.user_recordCounts.product_pins_count || 0,
                },
                {
                  title: 'Total Authentications',
                  value: this.user_recordCounts.response_count || 0,
                },
                {
                  title: 'User Feedback',
                  value: 0,
                },
                {
                  title: 'Total Airtime Distributed',
                  value: this.user_recordCounts.airtime_distributed || 0,
                },
              ];
            }
          });
        }
      });
    });
  }
  getOrderProfitChartSummary(): Observable<OrderProfitChartSummary[]> {
    return observableOf(this.summary);
  }

  getOrdersChartData(period: string): Observable<OrdersChart> {
    return observableOf(this.ordersChartService.getOrdersChartData(period));
  }

  getProfitChartData(period: string): Observable<ProfitChart> {
    return observableOf(this.profitChartService.getProfitChartData(period));
  }
}
