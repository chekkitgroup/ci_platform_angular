import { Injectable } from '@angular/core';
import { PeriodsService } from './periods.service';
import { OrdersChart, OrdersChartData } from '../data/orders-chart';
import { Subscription } from 'rxjs';
import { User } from '../../_models/user';
import { takeWhile } from 'rxjs/operators';
import { AuthenticationService } from '../../_services';
import { ProductService } from '../../_services/product.service';

@Injectable()
export class OrdersChartService extends OrdersChartData {

  private year = [
    '2012',
    '2013',
    '2014',
    '2015',
    '2016',
    '2017',
    '2018',
  ];
  week_lineData = [];
  week_periods = [];
  week_rawPeriods = [];
  week_rawPeriods1 = [];
  week_rawPeriods2 = [];

  month_lineData = [];
  month_periods = [];
  month_rawPeriods = [];
  month_rawPeriods1 = [];
  month_rawPeriods2 = [];

  year_lineData = [];
  year_periods = [];
  year_rawPeriods = [];
  year_rawPeriods1 = [];
  year_rawPeriods2 = [];
  private alive = true;
  private data = {};
  currentUserSubscription: Subscription;
  currentUser: User;
  constructor(private authenticationService: AuthenticationService,
    private period: PeriodsService, private _productService: ProductService) {
    super();
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
      _productService.getUserLineGRaphAnalytics(this.currentUser.id).pipe(takeWhile(() => this.alive))
        .subscribe((summary) => {
          const new_week_data = summary['data'].all_week_data;
          const new_week_data1 = summary['data'].valid_week_data;
          const new_week_data2 = summary['data'].invalid_all_week_data;
          this.computeWeekData(new_week_data, new_week_data1, new_week_data2);

          const new_mnth_data = summary['data'].all_month_data;
          const new_mnth_data1 = summary['data'].valid_all_month_data;
          const new_mnth_data2 = summary['data'].invalid_all_month_data;
          this.computeMonthData(new_mnth_data, new_mnth_data1, new_mnth_data2);

          const new_yr_data = summary['data'].all_year_data;
          const new_yr_data1 = summary['data'].valid_all_year_data;
          const new_yr_data2 = summary['data'].invalid_all_year_data;
          this.computeYearData(new_yr_data, new_yr_data1, new_yr_data2);
        });
    });
    this.getData();
  }
  async getData() {
    this.data = {
      week: await this.getDataForWeekPeriod(),
      month: await this.getDataForMonthPeriod(),
      year: await this.getDataForYearPeriod(),
    };
  }

  async computeWeekData(new_data, new_data1, new_data2) {
    await new_data.forEach(key => {
      this.week_periods.push(key.week_name);
      this.week_rawPeriods.push(key.value);
    });
    await this.week_lineData.push(this.week_rawPeriods);
    await new_data1.forEach(async key => {
      await this.week_rawPeriods1.push(key.value);
    });
    await this.week_lineData.push(this.week_rawPeriods1);

    await this.week_lineData.push(this.week_rawPeriods);
    await new_data2.forEach(async key => {
      await this.week_rawPeriods2.push(key.value);
    });
    await this.week_lineData.push(this.week_rawPeriods2);
    console.log('key object arary 1', await this.week_lineData);
    this.data = {
      week: await this.getDataForWeekPeriod(),
      month: await this.getDataForMonthPeriod(),
      year: await this.getDataForYearPeriod(),
    };
  }
  async computeMonthData(new_data, new_data1, new_data2) {
    await new_data.forEach(key => {
      this.month_periods.push(key.month_name);
      this.month_rawPeriods.push(key.value);
    });
    await this.month_lineData.push(this.month_rawPeriods);
    await new_data1.forEach(async key => {
      await this.month_rawPeriods1.push(key.value);
    });
    await this.month_lineData.push(this.month_rawPeriods1);

    await this.month_lineData.push(this.month_rawPeriods);
    await new_data2.forEach(async key => {
      await this.month_rawPeriods2.push(key.value);
    });
    await this.month_lineData.push(this.month_rawPeriods2);
    console.log('key object arary 2', await this.month_lineData);
    this.data = {
      week: await this.getDataForWeekPeriod(),
      month: await this.getDataForMonthPeriod(),
      year: await this.getDataForYearPeriod(),
    };
  }
  async computeYearData(new_data, new_data1, new_data2) {
    await new_data.forEach(key => {
      this.year_periods.push(key.year_name);
      this.year_rawPeriods.push(key.value);
    });
    await this.year_lineData.push(this.year_rawPeriods);

    await new_data1.forEach(async key => {
      await this.year_rawPeriods1.push(key.value);
    });
    await this.year_lineData.push(this.year_rawPeriods1);

    await this.year_lineData.push(this.year_rawPeriods);
    await new_data2.forEach(async key => {
      await this.year_rawPeriods2.push(key.value);
    });
    await this.year_lineData.push(this.year_rawPeriods2);
    console.log('key object arary 3', await this.year_lineData);
    this.data = {
      week: await this.getDataForWeekPeriod(),
      month: await this.getDataForMonthPeriod(),
      year: await this.getDataForYearPeriod(),
    };
  }
  private getDataForWeekPeriod(): OrdersChart {
    return {
      chartLabel: this.getDataLabels(6, this.getDateArray(this.week_periods)),
      linesData: this.week_lineData,
    };
  }
  private getDataForMonthPeriod(): OrdersChart {
    return {
      chartLabel: this.getDataLabels(4, this.getDateArray(this.month_periods)),
      linesData: this.month_lineData,
    };
  }

  private getDataForYearPeriod(): OrdersChart {
    return {
      chartLabel: this.getDataLabels(4, this.year_periods),
      linesData: this.year_lineData,
    };
  }

  getDataLabels(nPoints: number, labelsArray: string[]): string[] {
    const labelsArrayLength = labelsArray.length;
    const step = Math.round(nPoints / labelsArrayLength);

    return Array.from(Array(nPoints)).map((item, index) => {
      const dataIndex = Math.round(index / step);

      return index % step === 0 ? labelsArray[dataIndex] : '';
    });
  }

  getOrdersChartData(period: string): OrdersChart {
    return this.data[period];
  }
  getDateArray(arrayDAta) {
    const new_value = [];
    arrayDAta.forEach(element => {
      console.log("element", element);
      new_value.push(this.convertToWord(element));
    });
    return new_value;
  }
  convertToWord(value) {
    if (value) {
      const yrNo = value.split('/')[0];
      const mnthNo = Number(value.split('/')[1]);
      const month = new Array();
      month[0] = 'Jan';
      month[1] = 'Feb';
      month[2] = 'Mar';
      month[3] = 'Apr';
      month[4] = 'May';
      month[5] = 'Jun';
      month[6] = 'Jul';
      month[7] = 'Aug';
      month[8] = 'Sep';
      month[9] = 'Oct';
      month[10] = 'Nov';
      month[11] = 'Dec';
      if (mnthNo <= 4) {
        return month[0] + `/${yrNo}`;
      }
      if (mnthNo > 4 && mnthNo <= 8) {
        return month[1] + `/${yrNo}`;
      }
      if (mnthNo > 8 && mnthNo <= 13) {
        return month[2] + `/${yrNo}`;
      }
      if (mnthNo > 13 && mnthNo <= 17) {
        return month[3] + `/${yrNo}`;
      }
      if (mnthNo > 17 && mnthNo <= 21) {
        return month[4] + `/${yrNo}`;
      }
      if (mnthNo > 21 && mnthNo <= 26) {
        return month[5] + `/${yrNo}`;
      }
      if (mnthNo > 26 && mnthNo <= 30) {
        return month[6] + `/${yrNo}`;
      }
      if (mnthNo > 30 && mnthNo <= 34) {
        return month[7] + `/${yrNo}`;
      }
      if (mnthNo > 34 && mnthNo <= 39) {
        return month[8] + `/${yrNo}`;
      }
      if (mnthNo > 39 && mnthNo <= 43) {
        return month[9] + `/${yrNo}`;
      }
      if (mnthNo > 43 && mnthNo <= 47) {
        return month[10] + `/${yrNo}`;
      }
      if (mnthNo > 47 && mnthNo <= 52) {
        return month[11] + `/${yrNo}`;
      }
    }

  }
}
