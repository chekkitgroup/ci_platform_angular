import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { User } from '../../../_models/user';
import { LayoutService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../_services/authentication.service';
import { UserService } from '../../../@core/mock/users.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    // {
    //   value: 'cosmic',
    //   name: 'Cosmic',
    // },
    // {
    //   value: 'corporate',
    //   name: 'Corporate',
    // },
  ];

  currentTheme = 'default';

  userMenu = [];
  name: string;
  subtitle: string;
  currentUser: User;
  currentUserSubscription: Subscription;
  users: User[] = [];
  // this.authenticationService.currentUser.subscribe(x => this.currentUser = x);

  constructor(private sidebarService: NbSidebarService, private router: Router,
    private userService: UserService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private layoutService: LayoutService,
    private authenticationService: AuthenticationService,
    private breakpointService: NbMediaBreakpointsService) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      if (!this.currentUser.first_name) {
        this.currentUser.first_name = 'User';
      }
      this.userMenu.push({ title: `Hey ${this.currentUser.first_name} !`});
      this.userMenu.push({ title: `Profile`, icon: 'person' });
      this.userMenu.push({ title: `Log out`, icon: 'log-out' });
      console.log(this.currentUser);
      console.log('printed header nav');
    });
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
    this.menuService.onItemClick().subscribe((event) => {
      this.onItemSelection(event.item.title);
    });
  }
  onItemSelection(title: string) {
    console.log('yes', title);
    if (title === 'Profile') {
      this.profile();
    } else if (title === 'Log out') {
      this.logout();
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.router.navigate(['/pages/overview']);
    // this.menuService.navigateHome();
    return false;
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/pages/auth/login']);
  }
  profile() {
    this.router.navigate(['pages/profile']);
  }
}
