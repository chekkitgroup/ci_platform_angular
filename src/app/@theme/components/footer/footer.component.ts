import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by"><b>
      <a class="bottom-link" href="https://chekkitapp.com" target="_blank">Chekkit-</a></b> Copyright © 2020 </span>`,
})
export class FooterComponent {
}
