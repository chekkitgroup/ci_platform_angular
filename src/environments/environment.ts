export const environment = {
  production: false,
  // apiUrl: 'http://localhost:3000',
  apiUrl: 'http://54.191.192.249:3000',
  // apiUrl: 'https://ci-staging.chekkit.app',
  PORT: 3000,
  basePath: '/api/v1',
};
